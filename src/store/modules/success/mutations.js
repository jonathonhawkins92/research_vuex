import { SUCCESS } from './types';

const success = (state, obj) => {
  state.items = obj.items;
};

export default {
  [SUCCESS]: success,
};
