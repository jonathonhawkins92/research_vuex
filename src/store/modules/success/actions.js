import { SUCCESS } from './types';

const randomInt = (min, max) => min + max + Math.random();

const generate = ({ commit }, options = {}) => {
  const { min = 0, max = 100 } = options;
  // async mock
  setTimeout(() => {
    commit(SUCCESS, {
      items: randomInt(min, max),
    });
  }, 1000);
};

export default {
  generate,
};
