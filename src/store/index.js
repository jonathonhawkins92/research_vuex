/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';
import actions from './actions';
import login from './modules/login/index';

Vue.use(Vuex);

export const store = new Vuex.Store({
  namespaced: true,
  modules: {
    login,
  },
  actions,
});
