import * as types from './types';

// actions
const actions = {
  addToList({ commit }) {
    commit(types.ADD_TO_LIST);
  },
  updateInput({ commit }, input) {
    commit(types.UPDATE_INPUT, input);
  },
};

export default actions;
