import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const module = {
  state: {
    list: [
      { id: 0, text: 'Vegetables' },
      { id: 1, text: 'Cheese' },
      { id: 2, text: 'Whatever else humans are supposed to eat' },
    ],
    input: '',
  },
  getters,
  actions,
  mutations,
};

export default module;
