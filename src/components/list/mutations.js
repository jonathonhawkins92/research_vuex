import * as types from './types';

// mutations
const mutations = {
  [types.ADD_TO_LIST](state) {
    const listRef = state.list;
    const input = state.input;
    let id = listRef.slice(-1)[0].id;
    id += 1;
    state.list.push({
      id,
      text: input,
    });
    state.input = '';
  },
  [types.UPDATE_INPUT](state, input) {
    state.input = input;
  },
};

export default mutations;
